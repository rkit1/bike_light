#include "timer.h"
#include <Arduino.h>

void Timer::start(unsigned long _interval) {
    started_at = millis();
    interval = _interval;
    running = true;
}

bool Timer::done() {
    if (running && ((millis() - started_at) > interval)) {
        running = false;
        return true;
    }
    return false;
}