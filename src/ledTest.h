#pragma once
#include <Arduino.h>
#include "pwm.h"
#include "addrled.h"
#include "FastLED.h"

uint16_t k = 0;

void help() {
    Serial.println("Ввердите +, -, или число в диапазоне 0-999");
}

void method_loop() {
    while(Serial.available()) {
        auto c = Serial.peek();
        switch (c) {
        case '+':
            Serial.read();
            k++;
            break;
        case '-':
            Serial.read();
            if (k > 0) k--;
            break;
        case '\r':
        case '\n':
        case ' ':
            Serial.read();
            continue;
        default:
            auto i = Serial.parseInt();
            if (i >= 0)
                k = i;
            else {
                help();
            }
            break;
        }
        // PWM::set(k);
        LED::set(k, 400);
        pinMode(8, OUTPUT);
        FastLED.show();
        Serial.println(k);
    }
    FastLED.show();
}

void method_setup() {
    while(!Serial);
    help();
}

void hall_fall() {

}