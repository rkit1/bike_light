#pragma once
#include <Arduino.h>
#include "fir.h"
#include "timer.h"
#include "pwm.h"

///// Настрйки считывания скорости с магнитов.
// Минимальный возможный интервал между срабатываниями датчика (на максимальной скорости).
constexpr uint32_t min_delay = 20;
// Максимальный возможный интервал. Всё, что медленнее считаем за 0.
constexpr uint32_t max_delay = 200;

Fir_filter<uint16_t, 32> filter_speed;
volatile uint32_t last_fall = 0;
volatile uint32_t last_delay = max_delay;
Timer mainLoop;
Timer statsLoop;


void hall_fall() {
    auto ms = millis();
    last_delay = ms - last_fall;
    last_fall = ms;
}

void method_setup() {
    mainLoop.start(20);
    statsLoop.start(1000);
}

// c = 0,61803398874989484820458683436564
// для 1 / (0 + c) - c = 1
//     1 / (1 + c) - c = 0
void method_loop() {
    if(mainLoop.done()) {
        mainLoop.start(20);

        cli();
        auto delay = max(millis() - last_fall, last_delay);
        sei();

        auto delay1 = min(delay, max_delay) - min_delay;

        // Приводим гиперболу "1 / интервал" к скорости в диапазоне 0-1000
        auto speed = 1000000 / (delay1 * 1000 / (max_delay - min_delay) + 618) - 618;

        filter_speed.write(speed);
        PWM::set(filter_speed.read());

        if (statsLoop.done()) {
            statsLoop.start(1000);
            Serial.print("холл: ");
            Serial.print(delay);
            Serial.print(" скорость: ");
            Serial.print(speed);
            Serial.println();
        }
    }
}