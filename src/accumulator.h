#pragma once
#include <Arduino.h>
#include "timer.h"
#include "pwm.h"

// При каждом проходе магнита мимо датчика холла "заряд аккумулятора"
// увеиличивается на это значение до максимального заряда в 999.
constexpr float charge_per_pulse = 6;

// Каждую секунду батарея теряет такой процент от текущего заряда
constexpr float charge_decay_coeff = 20.0;



constexpr uint8_t tick_freq = 50;
constexpr float charge_decay_per_tick = pow(1 - charge_decay_coeff / 100, 1. / tick_freq);

volatile float charge_buffer = 0;
float charge = 0;
Timer mainLoop, statsLoop;

void hall_fall() {
    charge_buffer += charge_per_pulse;
}

void method_setup() {
    mainLoop.start(20);
    statsLoop.start(1000);
}

void method_loop() {
    if(mainLoop.done()) {
        mainLoop.start(1000 / tick_freq);

        cli();
        charge += charge_buffer / 10; // Подавляет мерцание на низких скоростях.
        charge_buffer = charge_buffer / 10 * 9 * charge_decay_per_tick;
        sei();

        PWM::set(charge);
        charge *= charge_decay_per_tick;

        if (statsLoop.done()) {
            statsLoop.start(1000);
            Serial.println(charge);
        }
    }
}