#pragma once
#include "pwm.h"
#include "FastLED.h"

namespace LED {
    constexpr uint8_t num_leds = 60*3;
    CRGB leds[num_leds];

    template<uint8_t fastled_pin> void setup() {
        FastLED.addLeds<WS2813, fastled_pin, BRG>(leds, 180);
        FastLED.setBrightness(20);
        FastLED.setCorrection(TypicalSMD5050);
        FastLED.setTemperature(SodiumVapor);
    }

    void set(uint32_t br, uint16_t input_point_range = 400, CRGB fgColor = CRGB::White, CRGB bgColor = CRGB::Black) {
        auto br2 = br * num_leds / input_point_range;
        auto rem = br * num_leds % input_point_range;
        auto percentile = rem * 255 / input_point_range;
        for (uint8_t i = 0; i < num_leds; i++) {
            if (i < br2) {
                leds[i] = fgColor;
            } 
            else if (i == br2) {
                leds[i] = fgColor;
                leds[i].fadeLightBy(255 - percentile);
            }
            else {
                leds[i] = CRGB::Black;
            }
        } 
        if (br > input_point_range) {
            PWM::set(br-input_point_range);
        } else {
            PWM::set(0);
        }
    }
}