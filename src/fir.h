#pragma once
#include <Arduino.h>

// FIR-фильтр для сглаживания шума.
// https://en.wikipedia.org/wiki/Finite_impulse_response
// T = тип значения, S = порядок фильтра.
template<typename T, uint8_t S> class Fir_filter {
    T data[S];
    uint8_t pos = 0;

    public:
    Fir_filter(T initial_value = 0) {
        for (auto i = 0; i < S; i++) {
            data[i] = initial_value;
        }
    }

    void write(T value) {
        data[pos++] = value;
        if (pos > S) pos = 0;
    }

    T read() {
        T out = 0;
        for (auto i = 0; i < S; i++) {
            out += data[i];
        }
        return out / S;
    }
};