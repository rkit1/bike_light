#include <Arduino.h>
#include "pwm.h"
#include "FastLED.h"


#include "accumulator2.h" // Свечение по принципу заряда аккумулятора
// #include "accumulator.h" // Свечение по принципу заряда аккумулятора
// #include "speed.h" // Свечение пропорционально скорости
// #include "ledTest.h" // Режим проверки светодиода


// Датчик холла на пине 2, затвор транзистора на пине 10
constexpr uint8_t hall_pin = 2;
constexpr uint8_t fastled_pin = 8;

void setup() {
    //LED::setup<fastled_pin>();
    Serial.begin(9600);
    pinMode(hall_pin, INPUT_PULLUP);
    pinMode(3, INPUT_PULLUP);
    pinMode(13, OUTPUT);
    attachInterrupt(digitalPinToInterrupt(hall_pin), hall_fall, FALLING);
    attachInterrupt(digitalPinToInterrupt(3), hall_fall, FALLING);
    PWM::setup();
    method_setup();
}

void loop() {
    method_loop();
}