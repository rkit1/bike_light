#pragma once
#include <Arduino.h>
#include "timer.h"
#include "addrled.h"

// При каждом проходе магнита мимо датчика холла "заряд аккумулятора"
// увеиличивается на это значение до максимального заряда в 999.
constexpr float charge_per_pulse = 10;

// Каждую секунду батарея теряет такой процент от текущего заряда
constexpr float charge_decay_coeff = 200.0;

constexpr uint8_t tick_freq = 50;
constexpr float charge_decay_per_tick = charge_decay_coeff / tick_freq;

volatile float charge_buffer = 0;
float charge = 0;
Timer mainLoop, statsLoop;

volatile bool blink = 0;
void hall_fall() {
    charge_buffer += charge_per_pulse;
    digitalWrite(13, blink);
    blink = !blink;
}

void method_setup() {
    mainLoop.start(20);
    statsLoop.start(1000);
}

void method_loop() {
    if(mainLoop.done()) {
        mainLoop.start(1000 / tick_freq);

        cli();
        charge += charge_buffer / 15; // Подавляет мерцание на низких скоростях.
        charge_buffer -= charge_buffer / 14;
        sei();
        if (charge > 1300) charge = 1300;
        PWM::set(charge);
        if (charge > charge_decay_per_tick) {
            charge -= charge_decay_per_tick;
        } else {
            charge = 0;
        }


        if (statsLoop.done()) {
            statsLoop.start(1000);
            Serial.print(charge_buffer);
            Serial.print('\t');
            Serial.println(charge);
        }
    } else {
        FastLED.show();
        yield();
    }
}